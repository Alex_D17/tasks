
#include <asm/uaccess.h>
#include <linux/pci.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/rtc.h>
#include <uapi/linux/time.h>



static ssize_t cur_time( int sec) {

	
   int h = sec/3600 % 24 + 3 ;
   int m = ((sec-(3600*h))/60) %60 ;
   int s = (sec - (3600*h) - (m*60))%60;
   printk("Current time - %d:%d:%d\n", h,m,s);
   return 0;
}


static ssize_t ch_format( int sec) {

	
   int h = (sec/3600);
   int m = (sec-(3600*h))/60;
   int s = (sec - (3600*h) - (m*60));
   printk( KERN_INFO "Changed format - %0d:%0d:%0d\n", h,m,s);
   return 0;
}
/*--------------------Module_param_cb()--------------------------------*/

int getUpTime=0;
int absTime(const char *val, const struct kernel_param *kp)
{
        int res = param_set_int(val, kp); // Use helper for write variable
        if(res==0) 
        {
        	
       	int j = jiffies_to_msecs(jiffies)/1000U;
        	int h = (j/3600);
   		int m = (j-(3600*h))/60;
   		int s = (j - (3600*h) - (m*60));
  		printk( KERN_INFO "Uptime - %0d:%0d:%0d\n", h,m,s);
                return 0;
        }
         return -1;
}
const struct kernel_param_ops my_param_ops = 
{
        .set = &absTime, // Use our setter ...
        .get = &param_get_int, // .. and standard getter
};
 
module_param_cb(getUpTime, &my_param_ops, &getUpTime, S_IRUGO|S_IWUSR );
/*----------------------------------------------------------------------------*/
