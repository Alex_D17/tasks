#include <linux/module.h>
#include <linux/jiffies.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <linux/pci.h>
#include <linux/version.h>
#include <linux/init.h>
#include "addFunc.h"

#define LEN_MSG 160

static u32 j, j_old;
static int counter=1;
MODULE_LICENSE("Dual BSD/GPL");

static int Format = 0;
module_param( Format, int, S_IRUGO );

#define IOFUNCS( name )                                                         \
static char buf_##name[ LEN_MSG + 1 ] = "не инициализировано "#name"\n";        \
static ssize_t SHOW_##name( struct class *class, struct class_attribute *attr,  \
                            char *buf ) {                                       \
   strcpy( buf, buf_##name );                                                   \
   printk( "read %ld\n", (long)strlen( buf ) );  				   \
   j = jiffies_to_msecs(jiffies)/1000U;					   \
   printk( KERN_INFO "module: jiffies on start = %d\n", j ); 		   \
   if (counter % 2==0)								   \
   {									          \
 		   \
 	if (Format == 1)							   \
 	{									   \
 	  ch_format(j - j_old);\
 					   \
 	}\
 	else\
 	{\
 	  printk( KERN_INFO "seconds since last read = %d\n", j - j_old); \
 	}					    	   \
    								   		   \
   }                                                   		           \
   counter++;									   \
   /*printk( KERN_INFO "test = %d\n",counter);*/					   \
   j_old = j;									   \
   return strlen( buf );                                                        \
}                                                                               \
static ssize_t STORE_##name( struct class *class, struct class_attribute *attr, \
                             const char *buf, size_t count ) {                  \
   printk( "write %ld\n", (long)count );                                        \
   strncpy( buf_##name, buf, count );                                           \
   buf_##name[ count ] = '\0';                                                  \
   return count;                                                                \
}

IOFUNCS( data1 );
#define OWN_CLASS_ATTR( name ) \
   struct class_attribute class_attr_##name = \
   __ATTR( name, ( S_IWUSR | S_IRUGO ), &SHOW_##name, &STORE_##name )
   
static OWN_CLASS_ATTR( data1 );
static struct class *x_class;



static int __init init( void ) {

   int res;
   x_class = class_create( THIS_MODULE, "x-class" );
   if( IS_ERR( x_class ) ) printk( "bad class create\n" );
   res = class_create_file( x_class, &class_attr_data1 );
   struct timespec64 time;
   unsigned long local_time;
   ktime_get_real_ts64(&time);
   local_time = time.tv_sec - (sys_tz.tz_minuteswest*60);
   cur_time(time.tv_sec);
   
   return 0;
}

void cleanup( void ) {
  
   //j1 = jiffies_to_msecs(jiffies) / 1000U; 
   //printk( KERN_INFO "module: jiffies on finish = %d\n", j1 );   
   //j = j1 - j;
   //printk( KERN_INFO "module: interval of life = %d\n", j ); 
   class_remove_file( x_class, &class_attr_data1 );
   class_destroy( x_class );  
   return;
}

module_init( init );
module_exit( cleanup );
