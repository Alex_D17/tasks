# Author
Alex170104

# Description

Exercise #13. Time Management



1. The number of ticks that have occurred since the system booted. And responding time of system(0.004), seted value 250 cycles per second .

![Image01](tick.png)


2. Timer reconfiguration after each expiration
![Image02](mytimer.png)


3. Module lifetime
![Image03](interval.png)

4. Calling callback function when timer expires and return kernel time of executing timer interrupt

![Image04](hrtimer.png)


