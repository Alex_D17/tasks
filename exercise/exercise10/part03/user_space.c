#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <string.h>
#include <sys/types.h>


#include <sys/stat.h>


#define SYS_DIR	 "/sys/class/x-class/xxx"

#define BUF_SIZE		100

static int sys_read()
{
	#define VALUE_MAX 30
	char path[VALUE_MAX];
	char value_str[3];
	int fd;
        char buf[BUF_SIZE];

       snprintf (buf, sizeof(buf), SYS_DIR);

        fd = open (buf, O_RDONLY);

	
        read(fd, &value_str, 3);


	close(fd);


	return(atoi(value_str));
	

}

int main(int argc, char *argv[]) {
	
	sys_read();
	printf("%d\n",sys_read());
	
	return EXIT_SUCCESS;
}




