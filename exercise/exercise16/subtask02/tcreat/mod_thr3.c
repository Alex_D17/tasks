#include <linux/module.h>
#include <linux/slab.h>
#include <linux/list.h>

#include <linux/fs.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <linux/pci.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/kthread.h>
#include <linux/delay.h> 
#include "../prefix.c"



static int N = 2;
MODULE_LICENSE( "GPL" );

#define LEN_MSG 160
static char buf_msg[ LEN_MSG + 1 ] = "";
struct list_head *iter, *iter_safe;
struct data *item;
LIST_HEAD( list );
 
struct data {
    int id;
    char msg;
    int length;
    int  amount_of_blocks;
    struct list_head list;
};
void pass_info(void);
/* sysfs show() method. Calls the show() method corresponding to the individual sysfs file */
static ssize_t xxx_show( struct class *class, struct class_attribute *attr, char *buf ) {

  int len=0;
  list_for_each( iter, &list ) {
  item = list_entry( iter, struct data, list );
  len+=sprintf( buf+len, "\n%d %c %d %d\n", item->id,item->msg, item->length,item->amount_of_blocks);
   pass_info();
  }
 
  return len;
}

static ssize_t xxx_store( struct class *class, struct class_attribute *attr, const char *buf, size_t count ) {

   strncpy( buf_msg, buf, count );
   buf_msg[ count ] = '\0';
   item = kmalloc( sizeof(*item), GFP_KERNEL );
   sscanf(buf_msg,"%d %c %d %d", &(item->id), &(item->msg), &(item->length),&(item->amount_of_blocks));
   list_add( &(item->list), &list );
   
   return -EIO;
    
}
CLASS_ATTR_RW( xxx );
static struct class *x_class;

int thread_fun1(void *data) 
{
   int i,j;
   int N = (int)data - 1;
   struct task_struct *t1 = NULL;
   printk( "%s is parent [%05d]\n", st( N ), current->parent->pid ); 
   for( j=0; j<item->amount_of_blocks; j++){
      for (i = 0; i < item->length; i++)
	    		{	
	    			printk("%c", item->msg);
	     			msleep(10);
	    		}
	    		printk("\n");
	    msleep(100);
	 }


   printk( "%s find signal!\n", st( N ) );
   if( t1 != NULL ) kthread_stop( t1 );   
   printk( "%s is completed\n", st( N ) );
   return 0;
    
}
void pass_info(void)
{
   struct task_struct *t1;
   int i;
   
   printk( "%smain process [%d] is running\n", sj(), current->pid );
    
   t1 = kthread_run( thread_fun1, (void*)N, "my_thread_%d", N );
   msleep(1);
   kthread_stop( t1 );   
   printk( "%smain process [%d] is completed\n", sj(), current->pid );
   
}
static int __init mod_init( void ) {
   int res;
   x_class = class_create( THIS_MODULE, "x-class" );
   if( IS_ERR( x_class ) ) printk( "bad class create\n" );
   res = class_create_file( x_class, &class_attr_xxx );
   return 0;
}
void x_cleanup(void) {
   class_remove_file( x_class, &class_attr_xxx );
   class_destroy( x_class );
   return;
}

         
module_init( mod_init );
module_exit( x_cleanup );
