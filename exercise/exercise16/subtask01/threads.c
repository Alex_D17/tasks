#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
 
#define ERROR_CREATE_THREAD -11
#define ERROR_JOIN_THREAD   -12
#define BAD_MESSAGE         -13
#define SUCCESS               0
 
typedef struct someArgs_tag {
    int id;
    char *msg;
    int length;
    int  amount_of_blocks;
} someArgs_t;
 
void* worker_thread(void *args) {
    someArgs_t *arg = (someArgs_t*) args;
    int del_print = 100;
    printf("\nThis is worker_thread #%d\n", arg->id);
    if (arg->msg == NULL) {
      printf("NUll message\n");
    }
    else{
    	for (int i = 0; i < arg->length; i++)
    		{	
    			
     			printf("%c", arg->msg);
     			while(del_print>0){del_print--;}
    		}
    }
    return SUCCESS;
}
 
#define NUM_THREADS 4
 
int main() {
    
    pthread_t threads[NUM_THREADS];
    int delay = 1000;
    int status;
    int i;
    int status_addr;
    int block_len = 1;
    someArgs_t args[NUM_THREADS];
    char *messages[] = {
        'A',
        NULL,
        'B',
        'C'
    };
    int lenght[4] = {1,2,3,4};
 
    for (i = 0; i < NUM_THREADS; i++) {
        args[i].id = i;
        args[i].msg = messages[i];
        args[i].length = lenght[i];
        args[i].amount_of_blocks = block_len;
    }
 
    for (i = 0; i < NUM_THREADS; i++) {
      while(delay>0){delay--;}
      delay = 1000;
        status = pthread_create(&threads[i], NULL, worker_thread, (void*) &args[i]);
        if (status != 0) {
            printf("main error: can't create thread, status = %d\n", status);
            exit(ERROR_CREATE_THREAD);
        }
    }
 
    printf("\nMain Message\n");
 
    for (i = 0; i < NUM_THREADS; i++) {
   	while(delay>0){delay--;}
        status = pthread_join(threads[i], (void**)&status_addr);
        if (status != SUCCESS) {
            printf("main error: can't join thread, status = %d\n", status);
            exit(ERROR_JOIN_THREAD);
        }
        printf("\njoined with address %d\n", status_addr);
    }
     return 0;
}
