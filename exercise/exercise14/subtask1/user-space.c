#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int max_value = 10;
int main ()
{
	clock_t start,finish;
	for (int i = 100000; i> 0; i = i*2)
	{	
		start = clock();
		int *arrayM = malloc(i);
		free(arrayM);
		finish = clock();
		if (arrayM == NULL)
		{
			printf("max value for malloc is - %d", i);
			break;		
		}
		printf("malloc allocation %d byted = %ld seconds\n", i, finish-start);
		
		start = clock();		
		int *arrayC = calloc(i,1);
		free(arrayC);
		finish = clock();
		if (arrayC == NULL)
		{
			printf("max value for calloc is - %d", i);
			break;		
		}
		printf("calloc allocation %d byted = %ld seconds\n", i, finish-start);
		start = clock();		
		int *arrayA = alloca(i);
		finish = clock();
		printf("alloca allocation %d byted = %ld seconds\n", i, finish-start);
	}
	

	return 0;

}

