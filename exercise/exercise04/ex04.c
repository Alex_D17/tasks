#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <time.h>

/////BLOCK OF LIST LISTING
#define container_of(ptr, type, member) (type *)( (char *)(ptr) - offsetof(type,member) )//marco 
struct list_node{
        struct list_node *next;
        struct list_node *prev;
};

struct list_node *list_add(struct list_node *new,struct list_node *next, struct list_node *prev) {
        new->next = next;
        new->prev = prev;
        return new;
}


/*struct list_node *list_del(struct list_node *pos) {

       if ( pos->next!= NULL) {
          pos->prev->next = pos->next;  
       }
   
	return 0;
}*/
/////BLOCK OF LIST DATA

struct information {
        struct list_node list;
        char* name;
        int result;
        int game_result;
        char* time_stamp;
};

//struct information - we will fill via  function <create_info>
struct information *create_info(char* name, int result, int game_result,char* time_stamp) {//tool for list creating

        struct information *info = malloc(sizeof(struct information));
        info->result = result;
        info->name = name;
        info->game_result = game_result;
        info->time_stamp = time_stamp;
        info->list.next =NULL;
	     info->list.prev = NULL;
        return info;
      
   };
int main(int argc, char **argv) { 

        struct list_node *head;
        //struct list_node *tail;
        struct list_node *cursor;
        struct information *element;
        struct list_node *prev;
        struct list_node *next;
        static int game_result = 0;
        static int i;
        int ch1=0;
        int result = 0;
	    
	     time_t current_time;
        char* c_time_string;
		

        prev = NULL;
        next = NULL;
        head = NULL;
        //tail = NULL;
        while(ch1!=3) {

            printf("Enter menu item number\n");
            printf("Enter 4 for help\n");
            scanf("%d",&ch1);

           if (ch1 ==1) {//play
        
	               if (head!= NULL) {
					
                     result = rand() % 7;
                     //taking time stamp
                     current_time = time(NULL);
                     c_time_string = ctime(&current_time);
                      //taked time stamp
                     game_result= game_result + result;
					 
                     element = create_info("player", result,game_result,c_time_string);
                     prev = cursor;
                     cursor->next = list_add(&element->list,next,prev);
                     cursor = cursor->next;
                     element = container_of(cursor->prev, struct information, list);
                      //printf("%s,%d,%d,%s\n", element->name, element->result, element->game_result, element->time_stamp);
                     // printf("%s,%d,%d,%s\n", cursor->prev->name, cursor->prev->result, cursor->prev->game_result, cursor->prev->time_stamp);
		  
	               }
	               else {

		               result = rand() % 7;
	                   //taking time stamp
	                  current_time = time(NULL);
	                  c_time_string = ctime(&current_time);
	                   //taked time stamp
	                  game_result= game_result + result;
					 
		               element = create_info("player", result,game_result,c_time_string);
		               head = list_add(&element->list,next,prev);
	                  cursor = head;
                     }
					 i++;
		      }
	     
            else if (ch1 == 2) {//Previous results
      
	                  for (; cursor!=NULL; cursor = cursor->prev)
                        {
                           element = container_of(cursor, struct information, list);
                           printf("%s,%d,%d,%s\n", element->name, element->result, element->game_result, element->time_stamp);
                        }
                     puts("------");
	         }  
            else if (ch1 == 3) {//exit
                     
	                  exit(0);

            }
            else {//help
            
	                 printf("--------\n");
                    printf("GAME - BONES\n");
                    printf("--------\n");
                    printf("Run with according arguments\n");
                    printf("--------\n");
                    printf("Menu\n");
                    printf("--------\n");      
                    printf("1-Play\n");
                    printf("2-Previous results\n");
                    printf("3-Exit right now\n");
            
            }
   }
}
  /*makefile
  bones: test.o
	gcc ./test.o
test.o: test.c
	gcc -Wall -c ./test.c

  
  */