# Author
Alex170104

# Description

Exercise #11. Character device driver

For communication with driver, we need to have special file of device, which is related to range of major and minor, which is processed by driver.
We can create this file with mknod command.

# Demo
## Subtask #1. Разработать модуль символьного устройства, предоставляющий пользователю только операцию чтения из устройства, с созданием именованного устройства вручную(Bash script src/part01)

1. выполнить сборку модуля + udevadm monitor;

![Image01](res/Image01.png)

![Image02](res/Image02.png)

2. подобрать вручную Major Number и выполнить загрузку модуля;
3. создать именованное устройство в каталоге /dev вручную и проверить работоспособность модуля;
4. создать именованное устройство в рабочем каталоге пользователя и проверить работоспособность модуля;
5. проверить поддержку драйвером заданного ему диапазона minor-номеров;
6. проверить загрузку модуля с определением номера устройства Major Number динамически.

![Image04](res/Image04.png)

##Subtask #2. Разработать модуль символьного устройства, динамически создающий устройства в каталоге /dev с заданным старшим и младшим номером. (Bash script src/part02)

0. Insert module + udevadm monitor;

![Image01](res/Image03.png)


1. проверить отсуствие обслуживаемых устройств в /dev до загрузки модуля;
2. проверить автоматическое создание устройств при загрузке модуля и их удаление при выгрузке;
![Image05](res/Image05.png)

3. проверить соответствующие элементы в /sys/module/ и /proc/ (/proc/devices и /proc/modules).

![Image06](res/Image06.png)

