# Author
Olexandr Diachuk

# Description

[Task #4](https://gl-khpi.gitlab.io/exercises/exercise08/).
Exercise #8. Linux Kernel: Overview, Structure, Building, Installing

# Demo
## Building
1. Get linux-stable kernel 5.13 and performed building:

![Image01](res/image01.png)

2. Gonfiguration and building rootfs:

![Image02](res/image02.png)

3. Display complete information about the built system (kernel version, user and host name, compiler version, build date and time).:

![Image03](res/image03.png)


