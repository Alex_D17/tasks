
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include<linux/moduleparam.h>
#include<linux/string.h>
#include <linux/fs.h>
#include <linux/cdev.h>

#include <linux/pci.h>
#include <linux/version.h>
#include <linux/init.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Alex170104");
MODULE_DESCRIPTION("Currency conversion module");
MODULE_VERSION("0.1");


#define MODULE_TAG      "Currency_module "
#define PROC_DIRECTORY  "Currency"
#define PROC_FILENAME   "buffer"
#define BUFFER_SIZE     30

static char *proc_buffer;
static size_t proc_msg_length;
static size_t proc_msg_read_pos;

static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file;

static ssize_t example_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset);
static ssize_t example_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset);

static const struct proc_ops  proc_fops = {
    .proc_read  = example_read,
    .proc_write = example_write,
};

/*------------Module_param_and_Module_param_cb()--------------------------------*/


static char* value="";
module_param(value, charp, 0);

int ConversionFactor=0;
int converter(const char *val, const struct kernel_param *kp)
{
        int res = param_set_int(val, kp); // Use helper for write variable
        int i=0;
        if(res==0) {
        	if (value[0] =='0')//euro->uah 
   		 {
   		 	     int new1 = *val-'0';
   		 	     int new2 = *(val+1)-'0';
   		 	     if (new2>0)
   		 	     {
   		 	     	new1=new1*10+new2;//conversion factor
   		 	     }
   		 	     int in1 = *proc_buffer-'0';
   		 	     int in2 = *(proc_buffer+1)-'0';
   		 	     if (in2>0)
   		 	     {
   		 	     	in1=in1*10+in2;//input amount of euro for conversion
   		 	     }
   		 	     
   		 	     int resu = new1*in1;
   		 	     sprintf(proc_buffer, "%d",resu);
   		 	     printk("Conversion result %d euro->uah: %s\n",in1,proc_buffer);
   		} 
   	      if (value[0]=='1')//uah->euro
     		{
     		 	int new1 = *val-'0';
   		 	int new2 = *(val+1)-'0';
   		 	if (new2)
   		 	{
   		 	    new1=new1*10+new2;//conversion factor
   		 	}
   		 	int in1 = *proc_buffer-'0';
   		 	int in2 = *(proc_buffer+1)-'0';
   		 	
   		        if (in2>0)
   		        {
   		            in1=in1*10+in2;//input amount of uah for conversion
   		           
   		        }
   		      
     		    
     			int resu = in1/new1;
     			int remainder = in1%new1;
     			if (remainder ==0)
     			{
     				sprintf(proc_buffer, "%d",resu);
     			}
     			if (remainder !=0)
     			{
   		 	        sprintf(proc_buffer, "%d.%d",resu,remainder);
   		 	}
   		 	printk("Conversion result %d uah->euro : %s\n",in1, proc_buffer);
     		
     			     		
     		
     		}   
        	
                printk(KERN_INFO "Conversion factor = %d\n", ConversionFactor);
                return 0;
        }
        return -1;
}
const struct kernel_param_ops my_param_ops = 
{
        .set = &converter, // Use our setter ...
        .get = &param_get_int, // .. and standard getter
};
 
module_param_cb(ConversionFactor, &my_param_ops, &ConversionFactor, S_IRUGO|S_IWUSR );
/*----------------------------------------------------------------------------*/




static int create_buffer(void)
{
    proc_buffer = kmalloc(BUFFER_SIZE, GFP_KERNEL);
    if (NULL == proc_buffer)
        return -ENOMEM;
    proc_msg_length = 0;

    return 0;
}


static void cleanup_buffer(void)
{
    if (proc_buffer) {
        kfree(proc_buffer);
        proc_buffer = NULL;
    }
    proc_msg_length = 0;
}


static int create_proc_example(void)
{
    proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
    if (NULL == proc_dir)
        return -EFAULT;

    proc_file = proc_create(PROC_FILENAME, S_IFREG | S_IRUGO | S_IWUGO, proc_dir, &proc_fops);
    if (NULL == proc_file)
        return -EFAULT;

    return 0;
}


static void cleanup_proc_example(void)
{
    if (proc_file)
    {
        remove_proc_entry(PROC_FILENAME, proc_dir);
        proc_file = NULL;
    }
    if (proc_dir)
    {
        remove_proc_entry(PROC_DIRECTORY, NULL);
        proc_dir = NULL;
    }
}


static ssize_t example_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset)
{
  
   size_t left;
  

    if (length > (proc_msg_length - proc_msg_read_pos))
        length = (proc_msg_length - proc_msg_read_pos);
   
    left = copy_to_user(buffer, &proc_buffer[proc_msg_read_pos], length);
    proc_msg_read_pos += length - left;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to read %u from %u chars\n", left, length);
    else
        printk(KERN_NOTICE MODULE_TAG "read %u chars\n", length);

    return length - left;
}


static ssize_t example_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset)
{
    size_t msg_length;
    size_t left;

    if (length > BUFFER_SIZE)
    {
        printk(KERN_WARNING MODULE_TAG "reduse message length from %u to %u chars\n", length, BUFFER_SIZE);
        msg_length = BUFFER_SIZE;
    }
    else
        msg_length = length;
    
    
    left = copy_from_user(proc_buffer, buffer, msg_length);

    proc_msg_length = msg_length - left;
    proc_msg_read_pos = 0;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to write %u from %u chars\n", left, msg_length);
    else
        printk(KERN_NOTICE MODULE_TAG "written %u chars\n", msg_length);

    return length;
}


static int __init example_init(void)
{
    int err;
    	
    err = create_buffer();
    if (err)
        goto error;

    err = create_proc_example();
    if (err)
        goto error;

    printk(KERN_NOTICE MODULE_TAG "loaded\n");
    return 0;

error:
    printk(KERN_ERR MODULE_TAG "failed to load\n");
    cleanup_proc_example();
    cleanup_buffer();
    return err;
}


static void __exit example_exit(void)
{
    cleanup_proc_example();
    cleanup_buffer();
    printk(KERN_NOTICE MODULE_TAG "exited\n");
}


module_init(example_init);
module_exit(example_exit);
