#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alex170104");
MODULE_DESCRIPTION("Currency converter");
MODULE_VERSION("0.1");

#define MODULE_TAG      "converter module"

static char* value="";
module_param(value, charp, 0);

static int __init example_init(void)
{
    int hrivnia = 100;
    int ConversionFactor = 31;
    int euro = 3;
    if (value[0]=='0')//uah->euro
    {
       int Quotient = hrivnia / ConversionFactor;
       int Remainder = hrivnia % ConversionFactor;
    	printk ("euro: %d.%d",Quotient, Remainder );
    }
    if (value[0]=='1')//euro->uah
    {
    	hrivnia = euro * ConversionFactor;
    	printk ("hrivnia : %d", hrivnia);
    }
    return 0;
}


static void __exit example_exit(void)
{
   printk(KERN_NOTICE MODULE_TAG " exited\n");
}


module_init(example_init);
module_exit(example_exit);

