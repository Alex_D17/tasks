#include <linux/module.h>
#define INCLUDE_VERMAGIC
#include <linux/build-salt.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

BUILD_SALT;

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__section(".gnu.linkonce.this_module") = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used __section("__versions") = {
	{ 0xeb4d7039, "module_layout" },
	{ 0x5b94bd36, "hrtimer_start_range_ns" },
	{ 0x4848926, "hrtimer_init" },
	{ 0x32a8584f, "gpiod_direction_output_raw" },
	{ 0x47229b5c, "gpio_request" },
	{ 0x792b3830, "spi_setup" },
	{ 0x543b1196, "spi_new_device" },
	{ 0x3023f084, "spi_busnum_to_master" },
	{ 0xbb78d31f, "class_create_file_ns" },
	{ 0x5daa17b1, "__class_create" },
	{ 0xf34fb773, "i2c_register_driver" },
	{ 0xbe1b7efd, "hrtimer_cancel" },
	{ 0xcd29798f, "spi_unregister_device" },
	{ 0xfe990052, "gpio_free" },
	{ 0xa3bd571d, "i2c_del_driver" },
	{ 0xe976676d, "class_destroy" },
	{ 0x98a585f3, "class_remove_file_ns" },
	{ 0x8e865d3c, "arm_delay_ops" },
	{ 0xc0025372, "hrtimer_forward" },
	{ 0xc5850110, "printk" },
	{ 0x9b4fea30, "gpiod_set_raw_value" },
	{ 0x89aab7a8, "gpio_to_desc" },
	{ 0x8f678b07, "__stack_chk_guard" },
	{ 0x86332725, "__stack_chk_fail" },
	{ 0x37bb83c, "spi_sync" },
	{ 0x5f754e5a, "memset" },
	{ 0xe7d9fa7d, "i2c_smbus_write_byte_data" },
	{ 0x611d2ce3, "_dev_err" },
	{ 0xfecc1d7f, "i2c_smbus_read_byte_data" },
	{ 0x97255bdf, "strlen" },
	{ 0x3c3ff9fd, "sprintf" },
	{ 0xad2811aa, "i2c_smbus_read_word_data" },
	{ 0xe4eb586c, "_dev_info" },
	{ 0xb1ad28e0, "__gnu_mcount_nc" },
};

MODULE_INFO(depends, "");

MODULE_ALIAS("i2c:mpu_dev");

MODULE_INFO(srcversion, "2AFE4EB434D0ED64ABD5942");
