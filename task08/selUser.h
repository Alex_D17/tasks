#include<linux/kernel.h>
#include<linux/init.h>
#include<linux/module.h>
#include<linux/moduleparam.h>
#include <linux/string.h>
int USER = 0;
int ch = 0;
/*----------------------Module_param_cb()--------------------------------*/

void print_info(int user)
{
    printk("here user %d reminder", user); 
    ch = user;
    list_for_each( iter, &list ) {
        item = list_entry( iter, struct data, list );
        if (*(item->user) == ch+ '0'){
         	timer_setup(&my_timer, my_timer_callback, 0);
       	mod_timer(&my_timer, jiffies + msecs_to_jiffies(item->delay));
       	printk(KERN_ALERT "user:%s, message:%s, delay in miliseconds: %ld", item->user,item->msg, item->delay);
        }     		
    }
}

int notify_param(const char *val, const struct kernel_param *kp)
{
    int res = param_set_int(val, kp); // Use helper for write variable
    if(!res) {
       switch (USER)
	{
	case 1:
		print_info(USER);
		break;
	case 2:
		print_info(USER);
		break;
        case 3:
		print_info(USER);
		break;
	case 4:
		print_info(USER);
		break;
	case 5:
		print_info(USER);
		break;
	default:
		printk("User not exist");
	}
               
    }
        return -1;
}
 
const struct kernel_param_ops my_param_ops = 
{
        .set = &notify_param, // Use our setter ...
        .get = &param_get_int, // .. and standard getter
};
 
module_param_cb(USER, &my_param_ops, &USER, S_IRUGO|S_IWUSR );
/*-------------------------------------------------------------------------*/

int cancelReminder = 0;
/*--------another---module_param---------------------------------------------------*/
int clear_param(const char *val, const struct kernel_param *kp)
{
      param_set_int(val, kp); // Use helper for write variable
      list_for_each_safe( iter, iter_safe, &list ) {
      item = list_entry( iter, struct data, list );
      if (*(item->user) == cancelReminder + '0'){
      		list_del( iter );
      		kfree( item );
      		printk("deleted");
      }
}
		return -1;
}


const struct kernel_param_ops my_params_ops = 
{
        .set = &clear_param, // Use our setter ...
        .get = &param_get_int, // .. and standard getter
};
 
module_param_cb(cancelReminder, &my_params_ops, &cancelReminder, S_IRUGO|S_IWUSR );

