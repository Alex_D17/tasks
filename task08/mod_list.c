#include <linux/module.h>
#include <linux/slab.h>
#include <linux/list.h>

#include <linux/fs.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <linux/pci.h>
#include <linux/version.h>
#include <linux/init.h>

#include <linux/kernel.h>
#include <linux/jiffies.h>
#include <linux/timer.h>




MODULE_LICENSE( "GPL" );

static int size = 5;
module_param( size, int, S_IRUGO | S_IWUSR );

#define LEN_MSG 160
static char buf_msg[ LEN_MSG + 1 ] = "";
struct list_head *iter, *iter_safe;
struct data *item;
LIST_HEAD( list );
 
struct data {
    char msg[30];
    char user[30];
    long delay;
    struct list_head list;
};

static struct timer_list my_timer;



/* sysfs show() method. Calls the show() method corresponding to the individual sysfs file */
static ssize_t xxx_show( struct class *class, struct class_attribute *attr, char *buf ) {

  int len=0;
  list_for_each( iter, &list ) {
      
      item = list_entry( iter, struct data, list );
      
      len+=sprintf( buf+len, "\n%s %s %ld\n", item->user,item->msg, item->delay);
     
   }
  return len;
}


void my_timer_callback(struct timer_list *timer) {
  printk(KERN_ALERT "called\n");}
 #include "selUser.h"
static ssize_t xxx_store( struct class *class, struct class_attribute *attr, const char *buf, size_t count ) {

   strncpy( buf_msg, buf, count );
   buf_msg[ count ] = '\0';
   item = kmalloc( sizeof(*item), GFP_KERNEL );
   sscanf(buf_msg,"%s  %s  %ld", item->user, item->msg, &(item->delay));
       
   list_add( &(item->list), &list );
   return -EIO;
    
}
CLASS_ATTR_RW( xxx );
static struct class *x_class;

static int __init mod_init( void ) {
   int res;
   x_class = class_create( THIS_MODULE, "x-class" );
   if( IS_ERR( x_class ) ) printk( "bad class create\n" );
   res = class_create_file( x_class, &class_attr_xxx );
   //timer_setup(&my_timer, my_timer_callback, 0);
   return 0;
}
void x_cleanup(void) {
   class_remove_file( x_class, &class_attr_xxx );
   class_destroy( x_class );
   del_timer(&my_timer);
   return;
}

         
module_init( mod_init );
module_exit( x_cleanup );
