#include "dev.h"
#include "selUser.h"
//#include "bufStatus.h"
#include <linux/cdev.h>
#include <linux/init.h>


static int major = 237;
module_param( major, int, S_IRUGO );

#define LEN_MSG 160


#define IOFUNCS( name )                                                         \
static char buf_##name[ LEN_MSG + 1 ] = "не инициализировано "#name"\n";        \
static ssize_t SHOW_##name( struct class *class, struct class_attribute *attr,  \
                            char *buf ) {                                       \
   strcpy( buf, buf_##name );                                                   \
   printk( "read %ld\n", (long)strlen( buf ) );                                 \
   return strlen( buf );                                                        \
}                                                                               \
static ssize_t STORE_##name( struct class *class, struct class_attribute *attr, \
                             const char *buf, size_t count ) {                  \
   printk( "write %ld\n", (long)count );                                        \
   strncpy( buf_##name, buf, count );                                           \
   buf_##name[ count ] = '\0';                                                  \
   return count;                                                                \
}



IOFUNCS( user );


#define OWN_CLASS_ATTR( name ) \
   struct class_attribute class_attr_##name = \
   __ATTR( name, ( S_IWUSR | S_IRUGO ), &SHOW_##name, &STORE_##name )

static OWN_CLASS_ATTR( user );


static struct class *users;

#define EOK 0
static int device_open = 0;

static int dev_open( struct inode *n, struct file *f ) {
   if( device_open ) return -EBUSY;
   device_open++;
   return EOK;
}

static int dev_release( struct inode *n, struct file *f ) {
   device_open--;
   return EOK;
}

static const struct file_operations dev_fops = {
   .owner = THIS_MODULE,
   .open = dev_open,
   .release = dev_release,
   .read  = dev_read,
   .write = dev_write,
  
};

#define DEVICE_FIRST  0
#define DEVICE_COUNT  1
#define MODNAME "my_cdev_dev"

static struct cdev hcdev;

static int __init dev_init( void ) {
   int res;
   users = class_create( THIS_MODULE, "users" );
   if( IS_ERR( users ) ) printk( "bad class create\n" );
   res = class_create_file( users, &class_attr_user );
   printk("'fixdev' module initialized\n");
   
   
   int ret;
   dev_t dev;
   if( major != 0 ) {
      dev = MKDEV( major, DEVICE_FIRST );
      ret = register_chrdev_region( dev, DEVICE_COUNT, MODNAME );
   }
   else {
      ret = alloc_chrdev_region( &dev, DEVICE_FIRST, DEVICE_COUNT, MODNAME );
      major = MAJOR( dev );  // не забыть зафиксировать!
   }
   if( ret < 0 ) {
      printk( KERN_ERR "=== Can not register char device region\n" );
      goto err;
   }
   cdev_init( &hcdev, &dev_fops );
   hcdev.owner = THIS_MODULE;
   ret = cdev_add( &hcdev, dev, DEVICE_COUNT );
   if( ret < 0 ) {
      unregister_chrdev_region( MKDEV( major, DEVICE_FIRST ), DEVICE_COUNT );
      printk( KERN_ERR "=== Can not add char device\n" );
      goto err;
   }
   printk( KERN_INFO "=========== module installed %d:%d ==============\n",
           MAJOR( dev ), MINOR( dev ) );
err:
   return ret;
}

static void __exit dev_exit( void ) {
   cdev_del( &hcdev );
   class_remove_file( users, &class_attr_user );
   class_destroy( users );
   unregister_chrdev_region( MKDEV( major, DEVICE_FIRST ), DEVICE_COUNT );
  
   printk( KERN_INFO "=============== module removed ==================\n" );
}
