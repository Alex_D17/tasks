#include<linux/kernel.h>
#include<linux/init.h>
#include<linux/module.h>
#include<linux/moduleparam.h>
#include <linux/string.h>
int USER = 0;
/*----------------------Module_param_cb()--------------------------------*/
int notify_param(const char *val, const struct kernel_param *kp)
{
        int res = param_set_int(val, kp); // Use helper for write variable
        if(!res) {
        int i=0;
	int j=0;
	switch (USER)
	{
	case 1:
		do{
		     printk("%c",*(hello_str+i) );
		     i++;
		   }while(*(hello_str+i)!='2' && *(hello_str+j) != '\0');
		break;
	case 2:
	    for (i = 0; *(hello_str+i) != '3'; i++)
		{
			if (*(hello_str+i) == '2')
			{
				j=i;
				while(*(hello_str+j) != '3'&& *(hello_str+j) != '\0')
				{
					printk("%c",*(hello_str+j));
					j++;
				}
			}
		}
		break;
	case 3:
		for (i = 0; *(hello_str+i) != '4'; i++)
		{
			if (*(hello_str+i) == '3')
			{
				j=i;
				while(*(hello_str+j) != '4'&& *(hello_str+j) != '\0')
				{
					printk("%c",*(hello_str+j));
					j++;
				}
			}
		}
		break;
	case 4:
			for (i = 0; *(hello_str+i) != '5' ; i++)
		{
			if (*(hello_str+i) == '4')
			{
				j=i;
				while(*(hello_str+j) != '5' && *(hello_str+j) != '\0')
				{
					printk("%c",*(hello_str+j));
					j++;
				}
			}
		}
		break;
	case 5:
		for (i = 0; *(hello_str+i) != '\0'; i++)
		{
			if (*(hello_str+i) == '5')
			{
				j=i;
				while(*(hello_str+j) != '\0')
				{
					printk("%c",*(hello_str+j));
					j++;
				}
			}
		}
		break;
	
		
	default:
		printk("User not exist");
	}
               
        }
        return -1;
}
 
const struct kernel_param_ops my_param_ops = 
{
        .set = &notify_param, // Use our setter ...
        .get = &param_get_int, // .. and standard getter
};
 
module_param_cb(USER, &my_param_ops, &USER, S_IRUGO|S_IWUSR );
/*-------------------------------------------------------------------------*/

int clearBuffer = 0;
/*--------another---module_param---------------------------------------------------*/
int clear_param(const char *val, const struct kernel_param *kp)
{
        int res = param_set_int(val, kp); // Use helper for write variable
	int i =0;
		for (i = 0; *(hello_str+i) != '\0' ; i++)
		{
			*(hello_str+i) = '\0';
			
		}
		return -1;
}


const struct kernel_param_ops my_params_ops = 
{
        .set = &clear_param, // Use our setter ...
        .get = &param_get_int, // .. and standard getter
};
 
module_param_cb(clearBuffer, &my_params_ops, &clearBuffer, S_IRUGO|S_IWUSR );

