# Author
Alex170104

# Description

Task #6. Implement a character device driver for text messaging between users.

For communication with driver, we need to have special file of device, which is related to range of major and minor, which is processed by driver.
We can create this file with mknod command.

Functions:

    to select users, and observe messages;
    to clear the buffer;
    to get the buffer status (size, number of messages, amount of used space).
    the default buffer size is 1 kB;



