#include <linux/fs.h>
#include <linux/init.h>
#include <linux/module.h>
#include <asm/uaccess.h>


MODULE_LICENSE( "GPL" );
MODULE_AUTHOR( "Oleg Tsiliuric <olej@front.ru>" );
MODULE_VERSION( "6.3" );


#define msg_len 1024 
static char hello_str[msg_len]= "";         // buffer!


void getStatus(void) {
   
   int i = 0;
   int UsedSpace= 0;
   static int message_counter = 0;
   while(hello_str[i]!= '\0')
   	{
   	 	UsedSpace++;
   		if (hello_str[i] == ':')
   		{
   		    message_counter++;
   		   
   		}
   		i++;
   	}
  	
   printk( KERN_INFO " message_counter : %d\n",message_counter  );  // EOF
   printk( KERN_INFO " free space left in bytes : %d\n", msg_len - UsedSpace  );  // EOF
   printk( KERN_INFO " used space in bytes : %d\n",UsedSpace  );  // EOF
  
}
static ssize_t dev_read( struct file * file, char * buf,
                           size_t count, loff_t *ppos ) {
   int len = strlen( hello_str );
   printk( KERN_INFO "=== read : %ld\n", (long)count );
   if( count < len ) return -EINVAL;
   if( *ppos != 0 ) {
      printk( KERN_INFO "=== read return : 0\n" );  // EOF
      return 0;
   }
   if( copy_to_user( buf, hello_str, len ) ) return -EINVAL;
   *ppos = count;
   
   printk( KERN_INFO "=== read return : %d\n", len );
   getStatus();
   return len;
}


static ssize_t dev_write (struct file * file, const char * buf,
			   size_t count, loff_t *ppos) {
   
    
    // copy_from_user(hello_str,buf,count);
    strcat(hello_str,buf);
   *ppos= count;
   printk( KERN_INFO "=== input message length : %ld\n", (long)count );
   int len = strlen( hello_str );	
   return  len;
}


static int __init dev_init( void );
module_init( dev_init );

static void __exit dev_exit( void );
module_exit( dev_exit );

