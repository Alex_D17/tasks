#include <stdio.h>

FILE *openFile(const char *filePath, const char *mode);
void closeFile(FILE **ptr);
